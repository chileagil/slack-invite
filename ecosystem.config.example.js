module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    {
      name      : "slack.chileagil.cl",
      script    : "bin/www",
      instances : 1,
      env: {
        "NODE_ENV":"production",        
      },
      env_production : {
        "SLACK_TOKEN": "TOKEN"
      }
    }
  ]
};
